import java.util.Scanner;

public class App {
    static Scanner sc = new Scanner(System.in);
    static Cat cat1 = new Cat("Dum");
    static Cat cat2 = new Cat("Som");
    static Cat cat3 = new Cat("Salid");
    static Minigame game = new Minigame();

    public static void process(String command) {
        switch (command) {
            case"p3":
                cat3.play();
                break;
            case"s3":
                cat3.sleep();
                break;
            case"e3":
                cat3.eat();
                break;
            case "c3":
                cat3.checkstatus();
                break;
            case"g1":
                game.game1();
                break;
            case"g2":
                game.game2();
                break;
            case "p1":
                cat1.play();
                break;
            case "s1":
                cat1.sleep();
                break;
            case "e1":
                cat1.eat();
                break;
            case "c1":
                cat1.checkstatus();
                break;
            case "h":
                System.out.println("press p1 for cat1 play");
                System.out.println("press s1 for cat 1sleep");
                System.out.println("press e1 for cat1 eat");
                System.out.println("press c1 for check status of cat1");
                
                System.out.println("press p2 for cat2 play");
                System.out.println("press s2 for cat2 sleep");
                System.out.println("press e2 for cat2 eat");
                System.out.println("press c2 for check status of cat2");

                System.out.println("press p3 for cat3 play");
                System.out.println("press s3 for cat3 sleep");
                System.out.println("press e3 for cat3 eat");
                System.out.println("press c3 for check status of cat3");

                System.out.println();
                System.out.println("press g1 For minigame 1");
                System.out.println("press g1 For minigame 2");
                System.out.println("press x to exit cat simulator");

                break;

            case "p2":
                cat2.play();
                break;
            case "s2":
                cat2.sleep();
                break;
            case "e2":
                cat2.eat();
                break;
            case "c2":
                cat2.checkstatus();
                break;

            case "x":
                System.out.println("Cat simulator will be exit");
                System.out.println("See ya");
                System.exit(0);
        }
    }
    public static void main(String[] args) {
        System.out.println("*************************");
        System.out.println("Welcome to cat simulator");
        System.out.println("*************************");
        System.out.println();
        while(true) {
            System.out.println("*************************");
            System.out.println("main menu");
            System.out.println("press h for help");
            System.out.println("*************************");
            String command = input();
            process(command);
            System.out.println();
            System.out.println();
            System.out.println();
        }
    }
    public static String input() {
        return sc.next();
    }
}

import java.util.Random;


public class Cat {
    private String name;
    private int lethargy; // ความง่วง
    private int fullness; // ความหิว
    private int happiness; // ความสุข

    public Cat(String name) { // Constructor เป็นตัวสร้าง ไม่มี return
        this.name = name;
        this.lethargy = 10;
        this.fullness = 10;
        this.happiness = 10;
    }
    public String getName(){
        return name;
    }
    public int getLethargy(){
        return lethargy;
    }

    public int getFullness(){
        return fullness;
    }

    public int getHappiness(){
        return happiness;
    }
    
    public void play() {
        

        if (fullness <= 0) {
            System.out.println("Your Cat " + getName() + " are died");
        } else {
            System.out.println(getName() + " is going to play");
            Random rand = new Random();
            for (int i = 0; i < 1; i++) {
                int r = rand.nextInt(3) + 1;
                if (r == 1){
                System.out.println("Whoaa this is so funn");
                System.out.println("happiness +");
                addHappiness();
                delFullness();
                delLethargy();
                }else if(r==2){
                    System.out.println("Just a chilling day");
                    System.out.println("happiness +");
                    addHappiness();
                    delFullness();
                    delLethargy();
                }else if(r==3){
                    System.out.println("It's a bad day");
                    System.out.println("happiness -");
                    delLethargy5();
                    delFullness();
                }
            }
            
        }
    }

    public void sleep() {
        if (fullness <= 0) {
            System.out.println("Your Cat are died");
        } else {
            Random rand = new Random();
            for (int i = 0; i < 1; i++) {
                int r = rand.nextInt(3) + 1;
                if (r == 1){
                    System.out.println("Zzzzzzzzzzz.");
                    addLethargy();
                }else if (r == 2){
                    System.out.println("Ah, I had a bad dream. So bad.");
                    System.out.println("Happiness -");
                    delHappiness();
                    addLethargy();
                }else if(r == 3){
                    System.out.println("When I was asleep, I dreamed that I ate a huge pile of salmon. It was delicious.");
                    System.out.println("Happiness +");
                    addHappiness();
                    addLethargy();
                }
            }
       
        }
    }

    public void eat() {
        if (fullness <= 0){
            System.out.println("Your cat are died");
        }else{
            Random rand = new Random();
            for (int i = 0; i < 1; i++) {
                int r = rand.nextInt(3) + 1;
                if (r == 1){
                    System.out.println("My owner poured food for them. I'm fine today");
                    addFullness();
                    addFullness();
                    delLethargy();
                
                }else if (r == 2){
                    System.out.println("I have to go out to hunt for rats. ");
                    System.out.println();
                    System.out.println("Ye caught it But not quite as full.");
                    addFullness();
                    addHappiness();
                    delLethargy();

                }else if(r == 3){
                    System.out.println("didn't eat anything");
                    System.out.println("Happiness -");
                    delHappiness();
                    delFullness();
            
            }
        }
        }
        
    }

    public void checkstatus() {
        System.out.println("******************");
        if (fullness <= 0) {
            System.out.println("Your Cat are died");
        } else {
            System.out.println("Check Status");
            System.out.println("Name = " + getName());
            System.out.println("Lethargy = " + getLethargy());
            System.out.println("fullness = " + getFullness());
            System.out.println("Happiness = " + getHappiness());
            System.out.println("******************");
        }

    }


    public int addHappiness() {
        happiness = happiness + 3;
        if (happiness >= 20) {
            happiness = 20;
            System.out.println("Your cat are fully happy");
        }
        return happiness;
    }

    public int delHappiness() {
        happiness = happiness + 5;
        if (happiness <= 0) {
            happiness = 0;
            System.out.println("Your cat is very sad");
        }
        return happiness;
    }

    public int addFullness() {
        fullness = fullness + 5;
        if (fullness >= 20){
            fullness = 20;
            System.out.println("Your cat has a bloated belly.");
        }
        return fullness;
    }

    public int delFullness() {
        fullness = fullness - 2;
        if (fullness < 6){
            System.out.println("Warning your cat are hungry feed it first before your cat died");
        }if(fullness <= 0) {
            fullness = 0 ;
            System.out.println("Your cat are Died");
        }
        return fullness;
    }
    public int addLethargy(){ //addความอิ่ม 10 หน่วย
        lethargy = lethargy + 10;
        if (lethargy >= 20){
            lethargy = 20;
            System.out.println("Your cat is so fresh Let's do an activity.");
        }
        return lethargy;
    }
    public int delLethargy() { //ความง่วงลดลง 2 หน่วย
        lethargy = lethargy - 2;
        if (lethargy <= 0) {
            System.out.println("Your cat is very sleepy. give him a break");
        } 
        return lethargy;
    }

    public int delLethargy5() { //ความง่วงลดลง 5 หน่วย
        lethargy = lethargy - 5;
        if (lethargy <= 0) {
            lethargy = 0;
            System.out.println("Your cat is very sleepy. give him a break");
        } 
        return lethargy;
    }

    

}

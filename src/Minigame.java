import java.util.Random;
import java.util.Scanner;

public class Minigame {
    private int num1;
    private int num2;
    private String playername;

    Minigame() {

    }

    public void game1() {
        Scanner sa = new Scanner(System.in);
        // int answer = sa.nextInt();

        Random rand = new Random();
        for (int i = 0; i < 1; i++) {
            int num1 = rand.nextInt(999) + 1;
            for (int j = 0; j < 1; j++) {
                int num2 = rand.nextInt(999) + 1;
                System.out.println(num1 + "+" + num2 + " =?");
                int result = num1+num2;
                int answer = sa.nextInt();
                if(answer == result){
                    System.out.println("Correct");
                }else{
                    System.out.println("Incorrect");
                }

            }
        }

    }

    public void game2() {
        Scanner sb = new Scanner(System.in);
        // int answer = sa.nextInt();

        Random rand = new Random();
        for (int i = 0; i < 1; i++) {
            int num1 = rand.nextInt(999) + 1;
            for (int j = 0; j < 1; j++) {
                int num2 = rand.nextInt(99) + 1;
                System.out.println(num1 + "*" + num2 + " =?");
                int result = num1*num2;
                int answer = sb.nextInt();
                if(answer == result){
                    System.out.println("Correct");
                }else{
                    System.out.println("Incorrect");
                }

            }
        }

    }

}
